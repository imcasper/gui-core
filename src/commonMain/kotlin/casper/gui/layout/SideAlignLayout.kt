package casper.gui.layout

import casper.geometry.Vector2d
import casper.types.Padding

class SideAlignLayout(val direction: Direction, val padding: Padding) : Layout {
	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>): Vector2d {
		return area
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>) {
		val upperCorner = Vector2d(padding.left.toDouble(), padding.top.toDouble())
		val lowerCorner = Vector2d(padding.right.toDouble(), padding.bottom.toDouble())

		val parentSize = area - upperCorner - lowerCorner

		for (child in children) {
			val childSize = child.getPreferredSize()
			when (direction) {
				Direction.LEFT -> {
					child.setPosition(upperCorner)
					child.setPreferredSize(Vector2d(childSize.x, parentSize.y))
				}
				Direction.RIGHT -> {
					child.setPosition(upperCorner + Vector2d(parentSize.x - childSize.x, 0.0))
					child.setPreferredSize(Vector2d(childSize.x, parentSize.y))
				}
				Direction.TOP -> {
					child.setPosition(upperCorner)
					child.setPreferredSize(Vector2d(parentSize.x, childSize.y))
				}
				Direction.BOTTOM -> {
					child.setPosition(upperCorner + Vector2d(0.0, parentSize.y - childSize.y))
					child.setPreferredSize(Vector2d(parentSize.x, childSize.y))
				}
			}
		}
	}
}