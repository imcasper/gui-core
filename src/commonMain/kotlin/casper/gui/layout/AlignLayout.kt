package casper.gui.layout

import casper.geometry.Vector2d

class AlignLayout(val dx: Align, val dy: Align, val gap: Double = 5.0) : Layout {

	override fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>):Vector2d {
		return area
	}

	override fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>){
		val gapX1 = Vector2d(gap)
		val gapX2 = Vector2d(gap * 2.0)

		for (child in children) {
			val sizeWithGap = child.getSize() + gapX2
			val positionWithGap = Vector2d(dx.getPosition(area.x, sizeWithGap.x), dy.getPosition(area.y, sizeWithGap.y))
			child.setPosition(positionWithGap + gapX1)
		}
	}
}