package casper.gui.layout

enum class Direction {
	LEFT,
	RIGHT,
	TOP,
	BOTTOM
}