package casper.gui.layout

import casper.geometry.Vector2d
import casper.gui.UINode
import casper.types.Padding

/**
 * 	Отвечает за расположение детей в контейнере
 */
interface Layout {
	companion object {
		val BORDER = BorderLayout(Vector2d.ZERO)

		//	Взаимное расположение детей
		val VERTICAL = OrientationLayout(Orientation.VERTICAL, Align.CENTER, Vector2d.ZERO, Vector2d.ZERO)
		val HORIZONTAL = OrientationLayout(Orientation.HORIZONTAL, Align.CENTER, Vector2d.ZERO, Vector2d.ZERO)

		//	Расположение в родителе
		val LEFT_TOP = AlignLayout(Align.MIN, Align.MIN, 0.0)
		val CENTER_TOP = AlignLayout(Align.CENTER, Align.MIN, 0.0)
		val RIGHT_TOP = AlignLayout(Align.MAX, Align.MIN, 0.0)
		val LEFT_CENTER = AlignLayout(Align.MIN, Align.CENTER, 0.0)
		val CENTER_CENTER = AlignLayout(Align.CENTER, Align.CENTER, 0.0)
		val RIGHT_CENTER = AlignLayout(Align.MAX, Align.CENTER, 0.0)
		val LEFT_BOTTOM = AlignLayout(Align.MIN, Align.MAX, 0.0)
		val CENTER_BOTTOM = AlignLayout(Align.CENTER, Align.MAX, 0.0)
		val RIGHT_BOTTOM = AlignLayout(Align.MAX, Align.MAX, 0.0)

		val STRETCH = StretchLayout(Padding.ZERO)

		val LEFT = SideAlignLayout(Direction.LEFT, Padding.ZERO)
		val RIGHT = SideAlignLayout(Direction.RIGHT, Padding.ZERO)
		val TOP = SideAlignLayout(Direction.TOP, Padding.ZERO)
		val BOTTOM = SideAlignLayout(Direction.BOTTOM, Padding.ZERO)
	}

	fun childrenLayout(area: Vector2d, children: Iterable<LayoutTarget>)
	fun selfLayout(area: Vector2d, children: Iterable<LayoutSource>):Vector2d
}

interface LayoutSource {
	fun getSize(): Vector2d
	fun getPreferredSize(): Vector2d
}

interface LayoutTarget : LayoutSource {
	fun setPreferredSize(size: Vector2d): UINode
	fun setPosition(position: Vector2d): UINode
}
//
//interface LayoutSource {
//	fun getPosition(): Vector2d
//	fun getSize(): Vector2d
//}