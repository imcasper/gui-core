package casper.gui.component.tab

import casper.geometry.Vector2i
import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.toggle.UIToggle
import casper.types.Texture

class UITab(val toggle: UINode, val content: UINode) {
	companion object {
		fun createWithText(uiScene: UIScene, caption: String, size: Vector2i, content: UINode): UITab {
			return UITab(UIToggle.createWithText(uiScene, caption).node.setSize(size), content)
		}

		fun createWithImage(uiScene: UIScene, texture: Texture, size: Vector2i, content: UINode): UITab {
			return UITab(UIToggle.createWithImage(uiScene, texture).node.setSize(size), content)
		}

		fun <Content : UIComponent> create(uiScene: UIScene, buttonContent: Content, size: Vector2i, content: UINode): UITab {
			return UITab(UIToggle.create(uiScene, buttonContent).node.setSize(size), content)
		}
	}
}