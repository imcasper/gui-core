package casper.gui.component.tab

import casper.collection.ObservableCollection
import casper.collection.observableListOf
import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.forProperty
import casper.gui.component.toggle.ToggleLogic
import casper.gui.getComponent
import casper.gui.getComponentOrNull
import casper.gui.layout.Direction
import casper.gui.util.BooleanGroupRule
import casper.gui.util.UITabController
import casper.signal.concrete.Signal
import casper.signal.concrete.StoragePromise
import casper.signal.util.then


class UITabMenu(node: UINode, val layoutInfo: TabMenuLayoutInfo, val tabs: ObservableCollection<UITab>, val logic: TabMenuLogic) : UIComponent(node) {
	private val tabMap = mutableMapOf<UITab, StoragePromise<Boolean>>()
	val toggleTarget = uiScene.createNode().setParent(node)
	val contentTarget = uiScene.createNode().setParent(node)
	val onTabDeselected = Signal<UITab>()
	val onTabSelected = Signal<UITab>()


	companion object {
		fun create(uiScene: UIScene, toggleSide: Direction = Direction.TOP, contentAlign: TabContentAlign = TabContentAlign(), rule: BooleanGroupRule = BooleanGroupRule.MAX_ONE_TRUE, vararg tabs: UITab): UITabMenu {
			val node = uiScene.createNode()
			return UITabMenu(node, TabMenuLayoutInfo(toggleSide, contentAlign), observableListOf(*tabs), TabMenuLogic(rule))
		}

		fun create(uiNode: UINode, toggleSide: Direction = Direction.TOP, contentAlign: TabContentAlign = TabContentAlign(), rule: BooleanGroupRule = BooleanGroupRule.MAX_ONE_TRUE, vararg tabs: UITab): UITabMenu {
			return UITabMenu(uiNode, TabMenuLayoutInfo(toggleSide, contentAlign), observableListOf(*tabs), TabMenuLogic(rule))
		}

		fun create(uiScene: UIScene, toggleSide: Direction = Direction.TOP, contentAlign: TabContentAlign = TabContentAlign(), rule: BooleanGroupRule = BooleanGroupRule.MAX_ONE_TRUE, tabs: ObservableCollection<UITab>): UITabMenu {
			val node = uiScene.createNode()
			return UITabMenu(uiScene.createNode(), TabMenuLayoutInfo(toggleSide, contentAlign), tabs, TabMenuLogic(rule))
		}

		fun create(uiNode: UINode, toggleSide: Direction = Direction.TOP, contentAlign: TabContentAlign = TabContentAlign(), rule: BooleanGroupRule = BooleanGroupRule.MAX_ONE_TRUE, tabs: ObservableCollection<UITab>): UITabMenu {
			return UITabMenu(uiNode, TabMenuLayoutInfo(toggleSide, contentAlign), tabs, TabMenuLogic(rule))
		}
	}


	init {
		forProperty<TabMenuStyle> {
			if (it.titleBack != null) {
				UIImage(toggleTarget, it.titleBack)
			}
			if (it.contentBack != null) {
				UIImage(contentTarget, it.contentBack)
			}
		}

		components.add(logic)
		node.isHitByArea = false
		contentTarget.isHitByArea = false

		tabs.then(components, ::addTab, ::removeTab)
		tabs.forEach(::addTab)

		node.propertySignal.then(components) { refresh() }
		refresh()
	}

	private fun refresh() {
		forProperty<TabMenuStyle> {
			node.layout = TabMenuLayout(layoutInfo, it, toggleTarget, contentTarget, onTabSelected)
		}
	}

	private fun addTab(tab: UITab) {
		val toggle = tab.toggle
		val onVisible = toggle.getComponent<ToggleLogic>().switch

		UITabController(toggle, onVisible, contentTarget, tab.content)
		logic.values.add(onVisible)
		tabMap.put(tab, onVisible)

		onVisible.then(components) {
			if (it) {
				onTabSelected.set(tab)
			} else {
				onTabDeselected.set(tab)
			}
		}

		toggleTarget += toggle
	}

	private fun removeTab(tab: UITab) {
		val toggle = tab.toggle
		toggle.getComponentOrNull<UITabController>()

		logic.values.remove(tab.toggle.getComponent<ToggleLogic>().switch)

		toggleTarget -= toggle
		tabMap.remove(tab)
	}

	fun deselectTab() {
		tabMap.values.forEach {
			it.set(false)
		}
	}

	fun selectTab(tab: UITab) {
		tabMap.get(tab)?.set(true)
	}

	fun getSelectedTab(): UITab? {
		tabMap.forEach {
			if (it.value.value) {
				return it.key
			}
		}
		return null
	}
}