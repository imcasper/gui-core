package casper.gui.component.scroll

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.box.UIBox
import casper.gui.layout.Direction
import casper.gui.layout.MapLayout
import casper.gui.layout.SideAlignLayout
import casper.gui.layout.StretchLayout
import casper.gui.update
import casper.signal.util.then
import casper.types.Padding
import kotlin.math.roundToInt

class UIScrollBox(node: UINode, val box: UIBox, val verticalScroll: ScrollLogic, val horizontalScroll: ScrollLogic) : UIComponent(node) {
	val onMove get() = box.mover.onMove
	val content get() = box.content

	private var showHorizontalScroll = false
	private var showVerticalScroll = false

	companion object {
		fun create(uiScene: UIScene): UIScrollBox {
			val node = uiScene.createNode()

			val vertical = UIScroll.create(uiScene, true)
			val horizontal = UIScroll.create(uiScene, false)

			val box = UIBox.create(uiScene)

			return UIScrollBox(node, box, vertical.logic, horizontal.logic)
		}
	}

	init {
		node += box.node

		box.node.onTransform.then(components) {
			invalidateScroll()
		}
		box.content.onTransform.then(components) {
			invalidateScroll()
			moveScrollByContent()
		}
		horizontalScroll.onValue.then(components) {
			moveContentByScroll()
		}
		verticalScroll.onValue.then(components) {
			moveContentByScroll()
		}

		invalidateScroll()
	}

	private fun invalidateScroll() {
		invalidateScrollVisibility()
		invalidateScrollState()
	}

	private fun invalidateScrollVisibility() {
		val contentSize = box.content.getSize()
		val selfSize = box.node.getSize()
		val showHorizontalScrollNext = contentSize.x > selfSize.x
		val showVerticalScrollNext = contentSize.y > selfSize.y

		if (showHorizontalScroll != showHorizontalScrollNext || showVerticalScroll != showVerticalScrollNext) {
			showHorizontalScroll = showHorizontalScrollNext
			showVerticalScroll = showVerticalScrollNext
			updateScrollVisibility()
		}
	}

	private fun invalidateScrollState() {
		val contentSize = box.content.getSize()
		val selfSize = box.node.getSize()

		if (showHorizontalScroll) {
			updateScrollState(horizontalScroll, selfSize.x * selfSize.x / contentSize.x, contentSize.x - selfSize.x)
		}
		if (showVerticalScroll) {
			updateScrollState(verticalScroll, selfSize.y * selfSize.y / contentSize.y, contentSize.y - selfSize.y)
		}
	}

	private fun updateScrollVisibility() {
		if (showHorizontalScroll) {
			node += horizontalScroll.node
		} else {
			node -= horizontalScroll.node
		}
		if (showVerticalScroll) {
			node += verticalScroll.node
		} else {
			node -= verticalScroll.node
		}
		setupLayout(showHorizontalScroll, showVerticalScroll)
	}

	private fun setupLayout(showHorizontalScroll: Boolean, showVerticalScroll: Boolean) {
		val horizontalScrollWidth = if (showHorizontalScroll) horizontalScroll.node.getSize().y.roundToInt() else 0
		val verticalScrollWidth = if (showVerticalScroll) verticalScroll.node.getSize().x.roundToInt() else 0

		val boxLayout = StretchLayout(Padding(0, 0, verticalScrollWidth, horizontalScrollWidth))
		val horizontalLayout = SideAlignLayout(Direction.BOTTOM, Padding(0, 0, verticalScrollWidth, 0))
		val verticalLayout = SideAlignLayout(Direction.RIGHT, Padding(0, 0, 0, horizontalScrollWidth))
		node.layout = MapLayout(mapOf(Pair(box.node, boxLayout), Pair(horizontalScroll.node, horizontalLayout), Pair(verticalScroll.node, verticalLayout)))
	}

	private fun updateScrollState(scroll: ScrollLogic, thickInPixels: Double, max: Double) {
		scroll.node.update<UIScroll> { render ->
			render.onThick.set(thickInPixels)
		}

		scroll.setMax(max)
	}

	private fun moveScrollByContent() {
		val position = box.content.getPosition()
		horizontalScroll.setValue(-position.x)
		verticalScroll.setValue(-position.y)
	}

	private fun moveContentByScroll() {
		val x = horizontalScroll.onValue.value
		val y = verticalScroll.onValue.value
		box.content.setPosition(-x, -y)
	}
}