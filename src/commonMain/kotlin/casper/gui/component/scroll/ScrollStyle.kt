package casper.gui.component.scroll

import casper.gui.UIProperty
import casper.gui.component.button.ButtonBackgroundStyle

class ScrollStyle(val width: Double, val preferredSliderThick: Double, val minSliderThick: Double, val backgroundStyle: ButtonBackgroundStyle, val sliderStyle: ButtonBackgroundStyle) : UIProperty {
	companion object {
		val DEFAULT = ScrollStyle(20.0, 20.0, 20.0, ButtonBackgroundStyle.DEFAULT, ButtonBackgroundStyle.DEFAULT)
	}
}