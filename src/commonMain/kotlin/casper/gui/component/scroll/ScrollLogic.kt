package casper.gui.component.scroll

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.math.clamp
import casper.signal.concrete.StorageFuture
import casper.signal.concrete.StorageSignal
import casper.signal.util.then
import kotlin.math.max
import kotlin.math.min

class ScrollLogic(node: UINode) : UIComponent(node) {
	companion object {
		fun create(uiScene: UIScene, value: Double = 0.0, min: Double = 0.0, max: Double = 1.0, handler:((Double)->Unit)? = null): ScrollLogic {
			return  create(uiScene.createNode(), value, min, max, handler)
		}

		fun create(uiNode: UINode, value: Double = 0.0, min: Double = 0.0, max: Double = 1.0, handler:((Double)->Unit)? = null): ScrollLogic {
			val logic = ScrollLogic(uiNode)
			logic.apply(value, min, max)
			if (handler != null) {
				logic.onValue.then  {
					handler(it)
				}
			}
			return logic
		}
	}

	private val _onMin = StorageSignal(0.0)
	private val _onValue = StorageSignal(0.0)
	private val _onMax = StorageSignal(1.0)

	val onMin: StorageFuture<Double>
		get() = _onMin

	val onMax: StorageFuture<Double>
		get() = _onMax

	val onValue: StorageFuture<Double>
		get() = _onValue

	fun setMin(value: Double) {
		apply(onValue.value, value, onMax.value)
	}

	fun setMax(value: Double) {
		apply(onValue.value, onMin.value, value)
	}

	fun setValue(value: Double) {
		apply(value, onMin.value, onMax.value)
	}

	fun apply(_value: Double, _min: Double, _max: Double) {
		val min = min(_min, _max)
		val max = max(_min, _max)
		val value = _value.clamp(min, max)

		_onMin.set(min)
		_onMax.set(max)
		_onValue.set(value)
	}
}