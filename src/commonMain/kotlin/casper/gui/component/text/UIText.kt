package casper.gui.component.text

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UITimer
import casper.gui.getProperty
import casper.gui.setProperty


class UIText(node: UINode, text: String) : UIComponent(node) {
	companion object {
		fun create(uiScene: UIScene, text: String): UIText {
			return UIText(uiScene.createNode(), text)
		}

		fun create(uiScene: UIScene, onText: () -> String): UIText {
			val textNode = UIText.create(uiScene, onText())
			UITimer.createRepeated(textNode.node, 100) {
				textNode.text = onText()
			}
			return textNode
		}
	}

	var text = text
		set(value) {
			field = value
			invalidate()
		}

	var fontProperty: FontProperty
		get() {
			return node.getProperty() ?: FontProperty.DEFAULT
		}
		set(value) {
			node.setProperty(value)
		}

	var formatProperty: TextFormatProperty
		get() {
			return node.getProperty() ?: TextFormatProperty.DEFAULT
		}
		set(value) {
			node.setProperty(value)
		}

	var colorProperty: TextColorProperty
		get() {
			return node.getProperty() ?: TextColorProperty.DEFAULT
		}
		set(value) {
			node.setProperty(value)
		}

	var alignProperty: TextAlignProperty
		get() {
			return node.getProperty() ?: TextAlignProperty.DEFAULT
		}
		set(value) {
			node.setProperty(value)
		}
}