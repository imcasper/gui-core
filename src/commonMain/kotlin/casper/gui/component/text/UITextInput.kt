package casper.gui.component.text

import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.getProperty
import casper.signal.concrete.StorageSignal
import casper.signal.util.then

class UITextInput(val logic: TextInputLogic) : UIComponent(logic.node) {
	companion object {
		fun create(uiScene: UIScene, firstText: String, listener: (String) -> Unit): UITextInput {
			val node = uiScene.createNode()
			val input = TextInputLogic(node, firstText)
			input.onText.then(listener)
			return UITextInput(input)
		}
	}

	val onFocused = StorageSignal(false)
	val onText = logic.onText
	val back = UIImage.create(uiScene, inputProperty.normalFill)

	init {
		components.add(back)
		node.mouseEventEnabled = true
		node.mouseEventCaptured = true
		onFocused.then(components) { isFocused ->
			back.fillStyle = if (isFocused) inputProperty.focusedFill else inputProperty.normalFill
		}
	}

	val fontProperty: FontProperty
		get() {
			return node.getProperty() ?: FontProperty.DEFAULT
		}

	val colorProperty: TextColorProperty
		get() {
			return node.getProperty() ?: TextColorProperty.DEFAULT
		}

	val alignProperty: TextAlignProperty
		get() {
			return node.getProperty() ?: TextAlignProperty.DEFAULT
		}

	val inputProperty: TextInputProperty
		get() {
			return node.getProperty() ?: TextInputProperty.DEFAULT
		}

}