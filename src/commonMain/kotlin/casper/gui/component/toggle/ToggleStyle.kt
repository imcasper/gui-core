package casper.gui.component.toggle

import casper.gui.UIProperty
import casper.gui.component.button.ButtonStyle

class ToggleStyle(val on: ButtonStyle, val off: ButtonStyle) : UIProperty {
	companion object {
		val DEFAULT = ToggleStyle(ButtonStyle.DEFAULT, ButtonStyle.DEFAULT)
	}
}