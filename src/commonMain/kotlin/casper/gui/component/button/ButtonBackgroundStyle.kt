package casper.gui.component.button

import casper.gui.UIProperty
import casper.types.Color4d
import casper.types.ColorFill
import casper.types.FillStyle

class ButtonBackgroundStyle(val out: FillStyle, val over: FillStyle, val pressOut: FillStyle, val pressOver: FillStyle, val disabledState: FillStyle) : UIProperty {
	companion object {
		val DEFAULT = ButtonBackgroundStyle(
				ColorFill(Color4d(0.3, 0.3, 0.3, 0.8)),
				ColorFill(Color4d(0.4, 0.4, 0.4, 0.8)),
				ColorFill(Color4d(0.6, 0.6, 0.6, 0.8)),
				ColorFill(Color4d(0.7, 0.7, 0.7, 0.8)),
				ColorFill(Color4d(0.3, 0.3, 0.3, 0.4)))
	}

	fun getBackFill(button: ButtonLogic): FillStyle {
		if (!button.isEnabled) {
			return disabledState
		} else if (!button.isPressed && !button.isFocused) {
			return out
		} else if (!button.isPressed && button.isFocused) {
			return over
		} else if (button.isPressed && !button.isFocused) {
			return pressOut
		} else if (button.isPressed && button.isFocused) {
			return pressOver
		} else {
			throw Error("Invalid state")
		}
	}
}