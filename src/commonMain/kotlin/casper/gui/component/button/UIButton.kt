package casper.gui.component.button

import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.forProperty
import casper.gui.component.text.UIText
import casper.gui.getProperty
import casper.gui.layout.Layout
import casper.gui.layout.MapLayout
import casper.gui.layout.StretchLayout
import casper.signal.util.then
import casper.types.Padding
import casper.types.ScaleTexture
import casper.types.Texture
import kotlin.math.roundToInt

typealias UIButtonWithImage = UIButton<UIImage>
typealias UIButtonWithLabel = UIButton<UIText>

fun UIButtonWithLabel.setText(value: String) {
	content.text = value
}

fun UIButtonWithImage.setImage(texture: Texture) {
	val p: ButtonStyle? = content.node.getProperty()
	content.fillStyle = ScaleTexture(texture, p?.contentGap ?: 0.0)
}

class UIButton<Content : UIComponent>(val logic: ButtonLogic, val content: Content) : UIComponent(logic.node) {
	companion object {
		fun <Content : UIComponent> create(uiScene: UIScene, content: Content, action: (() -> Unit)? = null): UIButton<Content> {
			val node = uiScene.createNode()
			val button = ButtonLogic(node, action)
			return UIButton(button, content)
		}

		fun createWithText(uiScene: UIScene, text: String, action: (() -> Unit)? = null): UIButtonWithLabel {
			return create(uiScene, UIText.create(uiScene, text), action)
		}

		fun createWithImage(uiScene: UIScene, texture: Texture, action: (() -> Unit)? = null): UIButtonWithImage {
			return create(uiScene, UIImage.create(uiScene, ScaleTexture(texture, 0.0)), action)
		}
	}

	private val back = UIImage.createEmpty(uiScene)


	init {
		node += back.node
		node += content.node

		components.add(back)
		components.add(content)
		logic.onEnabled.then(components) { refresh() }
		logic.onPressed.then(components) { refresh() }
		logic.onFocused.then(components) { refresh() }
		node.onInvalidate.then(components) { refresh() }
		node.propertySignal.then(components) { refresh() }

		refresh()
	}

	private fun refresh() {
		forProperty<ButtonStyle> {
			node.layout = MapLayout(mapOf(Pair(back.node, Layout.STRETCH), Pair(content.node, StretchLayout(Padding(it.contentGap.roundToInt())))))
			back.fillStyle = it.backgroundStyle.getBackFill(logic)
		}
	}
}