package casper.gui.component.button

import casper.gui.UIProperty

class ButtonStyle(val backgroundStyle: ButtonBackgroundStyle, val contentGap: Double) : UIProperty {
	companion object {
		val DEFAULT = ButtonStyle(ButtonBackgroundStyle.DEFAULT, 0.0)
	}
}