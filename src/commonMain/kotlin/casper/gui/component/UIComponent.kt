package casper.gui.component

import casper.core.DisposableHolder
import casper.gui.*

open class UIComponent(val node: UINode) : DisposableHolder() {
	val uiScene: UIScene get() = node.uiScene

	init {
		node.components.add(this)
	}

	override fun dispose() {
		super.dispose()
		node.components.remove(this)
	}

	fun invalidate() {
		node.invalidate()
	}

}

inline fun <reified Property : UIProperty> UIComponent.forProperty(action: (Property) -> Unit) {
	node.getProperty<Property>()?.let(action)
}

inline fun <reified Property : UIProperty> UIComponent.forPropertyOr(def: Property, action: (Property) -> Unit) {
	val prop = node.getProperty<Property>()
	if (prop != null) {
		action(prop)
	} else {
		action(def)
	}
}

inline fun <reified Component : UIComponent> getComponents(vararg nodes: UINode): Collection<Component> {
	val components = mutableListOf<Component>()
	nodes.forEach { node ->
		node.getComponentOrNull<Component>()?.let { component ->
			components.add(component)
		}
	}
	return components
}
