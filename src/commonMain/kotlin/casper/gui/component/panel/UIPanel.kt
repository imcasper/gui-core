package casper.gui.component.panel

import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.forProperty
import casper.signal.util.then
import casper.types.ColorFill
import casper.types.RED
import casper.types.setAlpha

class UIPanel(node: UINode) : UIComponent(node) {
	companion object {
		fun create(uiScene: UIScene): UIPanel {
			return UIPanel(uiScene.createNode())
		}
	}

	private val image = UIImage(node, ColorFill(RED.setAlpha(1.0)))

	init {
		node.mouseEventCaptured = true
		node.onInvalidate.then(components) { refresh() }
		refresh()
	}

	private fun refresh() {
		forProperty<PanelProperty> { image.fillStyle = it.fillStyle }
	}
}