package casper.gui.component.box

import casper.geometry.Vector2d
import casper.gui.UINode
import casper.gui.component.UIComponent
import casper.signal.util.then

class UIContentToNodeLimit(node: UINode, val content: UINode) : UIComponent(node) {

	init {
		components.add(content)
		content.onTransform.then(components) { update() }
		node.onTransform.then(components) { update() }
	}

	private fun update() {
		var position = content.getPosition()
		position = position.upper(Vector2d.ZERO)
		position = position.lower(node.getSize() - content.getSize())
		content.setPosition(position)
	}
}