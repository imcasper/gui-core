package casper.types

class Texture (val url:String) {
	override fun toString(): String {
		return "Texture(url='$url')"
	}
}