package casper.types

data class Padding(val left: Int, val top: Int, val right: Int, val bottom: Int) {
	companion object {
		val ZERO = Padding(0, 0, 0, 0)
	}

	constructor(border: Int) : this(border, border, border, border)
}
